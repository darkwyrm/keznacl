# keznacl

keznacl is an MIT-licensed library written in Kotlin for making work with cryptography easier by
providing an easy-to-use API for public key encryption, secret key encryption, digital signatures,
data hashing, and password hashing. It started as a general port of other implementations which have
preceded it, including [Go](https://gitlab.com/darkwyrm/goeznacl)
and [Rust](https://gitlab.com/darkwyrm/eznacl), but it has evolved even further for greater
convenience and ease-of-use.

## Description

Cryptography is really hard. Any code which implements it is equally hard. Anything which touches
the implementation code isn't much easier. The original concept for this library came from a need to
work with crypto keys over a text-based protocol, but as it matured, I discovered it also made
debugging code which interacts with cryptography much easier, too.

**Please** don't use this code to place *important* crypto keys in your code or embed backdoors. No
one needs that kind of drama.

## Contributing and Support

Questions and contributions are always welcome, should you feel so inclined.

## Usage

The library has multiple types of documentation to get you started. If you're in a hurry, there is a
file of examples in [src/test/kotlin/keznacl](https://gitlab.com/mensago/keznacl/-/blob/main/lib/src/test/kotlin/keznacl/Examples.kt?ref_type=heads). All of the source code is well-commented.

For full library documentation, check out the repository, open the command line in the repository
root, and run `./gradlew.bat dokkaHtml` (Windows) or `./gradlew dokkaHtml` (everything else). Once
the command completes, navigate to `lib/build/dokka/html` to find the newly-built documentation.

### A Quick Overview

- [CryptoString](https://gitlab.com/mensago/keznacl/-/blob/main/lib/src/main/kotlin/keznacl/CryptoString.kt?ref_type=heads) -
  A new data type class which pairs an algorithm with Base85-encoded binary data.


- [EncryptionPair](https://gitlab.com/mensago/keznacl/-/blob/main/lib/src/main/kotlin/keznacl/Encryption.kt?ref_type=heads) -
  A keypair for public-key (asymmetric) encryption
- [EncryptionKey](https://gitlab.com/mensago/keznacl/-/blob/main/lib/src/main/kotlin/keznacl/Encryption.kt?ref_type=heads) -
  Just the public key half of an encryption keypair


- [SigningPair](https://gitlab.com/mensago/keznacl/-/blob/main/lib/src/main/kotlin/keznacl/Signing.kt?ref_type=heads) -
  A keypair for digital signatures
- [VerificationKey](https://gitlab.com/mensago/keznacl/-/blob/main/lib/src/main/kotlin/keznacl/Signing.kt?ref_type=heads) -
  The public half of a signing keypair

- [SecretKey](https://gitlab.com/mensago/keznacl/-/blob/main/lib/src/main/kotlin/keznacl/SecretKey.kt?ref_type=heads) -
  Symmetric encryption


- [hash() / hashFile()](https://gitlab.com/mensago/keznacl/-/blob/main/lib/src/main/kotlin/keznacl/Hashing.kt?ref_type=heads) -
  Fast data hashing
- [Argon2idPassword](https://gitlab.com/mensago/keznacl/-/blob/main/lib/src/main/kotlin/keznacl/Argon2Password.kt?ref_type=heads) -
  Safe password hashing

If you work with encrypted JSON extensively, the
functions [serializeAndEncrypt()](https://gitlab.com/mensago/keznacl/-/blob/main/lib/src/main/kotlin/keznacl/JSONSupport.kt?ref_type=heads)
and  [decryptAndDeserialize()](https://gitlab.com/mensago/keznacl/-/blob/main/lib/src/main/kotlin/keznacl/JSONSupport.kt?ref_type=heads)
come in awfully handy.

### Algorithm Support

Some use cases will require support for specific or multiple algorithms. For these needs are the
functions `getSupportedAsymmetricAlgorithms()`, `getSupportedSecretAlgorithms()`, `getSupportedSigningAlgorithms()`,
and `getSupportedHashAlgorithms()`, which return a list of strings containing the names of all
algorithms supported. There are corresponding
calls `getPreferredAsymmetricAlgorithm()`, `getPreferredSecretAlgorithm()`, `getPreferredSigningAlgorithm()`,
and `getPreferredHashAlgorithm()` for instances where the library chooses for you if you're not sure
which one is best.

As of this writing, keznacl supports the following algorithms:

- Symmetric (Secret Key) Encryption
  - XSALSA20*
- Asymmetric (Public Key) Encryption
  - CURVE25519*
- Digital Signatures
  - ED25519*
- Cryptographic Hashes
  - BLAKE2B-256*
  - SHA-256
- Password Hashes
  - Argon2id*
  - Argon2i
  - Argon2d
  -

Algorithms with an asterisk attached to them are the algorithms returned
by `getPreferred*Algorithm()` calls. Support for AES encryption is planned. RSA support is not,
however.
