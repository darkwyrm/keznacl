import org.jetbrains.dokka.gradle.DokkaTask

version = "1.0.0"

plugins {
    // Apply the org.jetbrains.kotlin.jvm Plugin to add support for Kotlin.
    alias(libs.plugins.jvm)
    kotlin("plugin.serialization") version "1.9.0"
    `jvm-test-suite`

    // Add in Dokka for documentation generation
    id("org.jetbrains.dokka") version "1.9.20"

    // Apply the java-library plugin for API and implementation separation.
    `java-library`
    `maven-publish`
}

repositories {
    mavenCentral()
    maven {
        // This repo is needed for Argon2
        setUrl("https://maven.scijava.org/content/repositories/public/")
    }
}

dependencies {
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.6.2")
    implementation("com.github.alphazero:Blake2b:bbf094983c")
    implementation("org.purejava:tweetnacl-java:1.1.2")
    implementation("de.mkammerer:argon2-jvm:2.11")

    testImplementation("org.jetbrains.kotlin:kotlin-test-junit5")
    testImplementation(libs.junit.jupiter.engine)

    testRuntimeOnly("org.junit.platform:junit-platform-launcher")
}

// Apply a specific Java toolchain to ease working on different environments.
java {
    toolchain {
        languageVersion = JavaLanguageVersion.of(17)
    }
    withSourcesJar()
    withJavadocJar()
}

// Task registration and config

tasks.register<Jar>("dokkaHtmlJar") {
    dependsOn(tasks.dokkaHtml)
    from(tasks.dokkaHtml.flatMap { it.outputDirectory })
    archiveClassifier.set("html-docs")
}

tasks.register<Jar>("dokkaJavadocJar") {
    dependsOn(tasks.dokkaJavadoc)
    from(tasks.dokkaJavadoc.flatMap { it.outputDirectory })
    archiveClassifier.set("javadoc")
}

tasks.named<Test>("test") {
    useJUnitPlatform()
}

tasks.withType<Jar>().configureEach {
    archiveBaseName.set(rootProject.name)
    archiveVersion.set(version.toString())

    manifest {
        attributes(
            mapOf(
                "Implementation-Title" to rootProject.name,
                "Implementation-Version" to project.version
            )
        )
    }
}

tasks.jar {
    dependsOn(tasks.named("dokkaJavadocJar"))
    dependsOn(tasks.named("sourcesJar"))
}

tasks.withType<DokkaTask>().configureEach {
    moduleName.set(project.name)
    moduleVersion.set(project.version.toString())
    suppressObviousFunctions.set(true)
    suppressInheritedMembers.set(false)
    offlineMode.set(true)

    val dokkaBaseConfiguration = """
    {
      "footerMessage": "©️2024 Jon Yoder"
    }
    """
    pluginsMapConfiguration.set(
        mapOf(
            // fully qualified plugin name to json configuration
            "org.jetbrains.dokka.base.DokkaBase" to dokkaBaseConfiguration
        )
    )

    dokkaSourceSets {
        configureEach {
            moduleName.set("keznacl")
            displayName.set("keznacl")
            jdkVersion.set(17)
            includes.from(project.files(), "module.md")
        }
    }
}
