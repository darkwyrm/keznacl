# Module keznacl

## Prerequisites

Read up on the storage format class, CryptoString

## Encryption

- Public Key (Asymmetric) Encryption: [EncryptionPair]
- Secret Key (Symmetric) Encryption: [SecretKey]

## Hashing

- Regular data hashes: [hash()]
- Password hashes: [Password]
