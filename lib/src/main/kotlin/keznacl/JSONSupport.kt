package keznacl

import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.security.GeneralSecurityException

/**
 * Serializes an object to JSON and returns the encrypted version of it.
 *
 * @exception kotlinx.serialization.SerializationException Returned for encoding-specific errors
 * @exception IllegalArgumentException Returned if the encoded input does not comply format's
 * specification
 * @exception GeneralSecurityException Returned for errors during the encryption process.
 */
inline fun <reified T> serializeAndEncrypt(obj: T, key: Encryptor): CryptoString {
    return key.encrypt(Json.encodeToString(obj).encodeToByteArray())
}

/**
 * Serializes an object to JSON and returns the encrypted version of it.
 *
 * @exception kotlinx.serialization.SerializationException Returned for encoding-specific errors
 * @exception IllegalArgumentException Returned if the encoded input does not comply format's
 * specification
 * @exception GeneralSecurityException Returned for errors during the encryption process.
 */
inline fun <reified T> decryptAndDeserialize(cs: CryptoString, key: Decryptor): T {
    val rawJSON = key.decrypt(cs).decodeToString()
    return Json.decodeFromString<T>(rawJSON)
}
