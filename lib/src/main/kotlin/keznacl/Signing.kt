package keznacl

import com.iwebpp.crypto.TweetNaclFast.Signature
import kotlinx.serialization.Serializable

/** Returns true if the string represents a supported digital signing algorithm */
fun isSupportedSigning(s: String): Boolean {
    return s.uppercase() == "ED25519"
}

/**
 * Returns the digital signing algorithms supported by the library. Currently the only supported
 * algorithm is ED25519. Other algorithms may be added at a future time.
 */
fun getSupportedSigningAlgorithms(): List<CryptoType> {
    return listOf(CryptoType.ED25519)
}

/**
 * Returns the recommended digital signature algorithm supported by the library. If you don't know
 * what to choose for your implementation, this will provide a good default which balances speed and
 * security.
 */
fun getPreferredSigningAlgorithm(): CryptoType {
    return CryptoType.ED25519
}

/**
 * The SigningPair class represents a cryptographic keypair used for creating and verifying digital
 * signatures.
 */
@Serializable
class SigningPair private constructor(val pubKey: CryptoString, val privKey: CryptoString) :
    Verifier, KeyPair {

    override fun getPublicKey(): CryptoString {
        return pubKey
    }

    override fun getPrivateKey(): CryptoString {
        return privKey
    }

    override fun getPublicHash(algorithm: CryptoType): Hash {
        return pubKey.hash(algorithm)
    }

    override fun getPrivateHash(algorithm: CryptoType): Hash {
        return privKey.hash(algorithm)
    }

    /**
     * Creates a digital signature of the data supplied to it.
     *
     * @exception IllegalArgumentException Returned if there was a Base85 decoding error
     * @exception SigningFailureException Returned if there was an error signing the data
     */
    fun sign(data: ByteArray): CryptoString {
        if (data.isEmpty()) throw EmptyDataException()

        // TweetNaCl expects the private key to be 64-bytes -- the first 32 are the private key and
        // the last are the public key. We don't do that here -- they are split because they are
        // associated a different way.
        val rawKey = Base85.decode(privKey.encodedData + pubKey.encodedData)

        val box = Signature(null, rawKey)
        val signature = box.detached(data)

        if (signature == null || signature.isEmpty())
            throw SigningFailureException()

        return CryptoString.fromBytes(CryptoType.ED25519, signature)!!
    }

    override fun toString(): String {
        return "$pubKey,$privKey"
    }

    /**
     * Verifies the given signature for the data passed and returns true if it verifies and false if
     * it does not.
     *
     * @exception AlgorithmMismatchException Returned if the signature algorithm does not match that
     * of the SigningPair object.
     */
    override fun verify(data: ByteArray, signature: CryptoString): Boolean {
        if (data.isEmpty()) throw EmptyDataException()
        if (signature.prefix != pubKey.prefix) throw AlgorithmMismatchException()

        val rawPubKey = pubKey.toRaw()
        val rawSignature = signature.toRaw()
        val box = Signature(rawPubKey, null)

        return box.detached_verify(data, rawSignature)
    }

    companion object {

        /**
         * Creates a SigningPair from the specified CryptoStrings.
         *
         * @exception KeyErrorException Returned if given keys using different algorithms
         * @exception UnsupportedAlgorithmException Returned if the library does not support the
         * algorithm specified by the keys
         */
        fun from(pubKeyStr: CryptoString, privKeyStr: CryptoString): SigningPair {

            if (pubKeyStr.prefix != privKeyStr.prefix)
                throw KeyErrorException()
            if (!isSupportedSigning(pubKeyStr.prefix))
                throw UnsupportedAlgorithmException()

            return SigningPair(pubKeyStr, privKeyStr)
        }

        /**
         * Creates a SigningPair from two strings containing data in [CryptoString] format.
         *
         * @exception BadValueException Returned if either key contains invalid data
         * @exception KeyErrorException Returned if given keys using different algorithms
         * @exception UnsupportedAlgorithmException Returned if the library does not support the
         * algorithm specified by the keys
         */
        fun fromStrings(pubKeyStr: String, privKeyStr: String): SigningPair {

            val publicKeyCS = CryptoString.fromString(pubKeyStr)
                ?: throw BadValueException("bad public key")
            val privateKeyCS = CryptoString.fromString(privKeyStr)
                ?: throw BadValueException("bad private key")
            return from(publicKeyCS, privateKeyCS)
        }

        /**
         * Generates a new signing keypair using the specified algorithm.
         *
         * @exception KeyErrorException Returned if there was a problem generating the keypair
         * @exception UnsupportedAlgorithmException Returned if the library does not support the
         * algorithm specified
         */
        fun generate(algorithm: CryptoType = getPreferredSigningAlgorithm()): SigningPair {

            when (algorithm) {
                CryptoType.ED25519 -> {
                    val keyPair = Signature.keyPair()
                    val publicKeyCS = CryptoString.fromBytes(CryptoType.ED25519, keyPair.publicKey)
                        ?: throw KeyErrorException()

                    val privateKeyCS =
                        CryptoString.fromBytes(
                            CryptoType.ED25519,
                            keyPair.secretKey.copyOfRange(0, 32)
                        ) ?: throw KeyErrorException()
                    return from(publicKeyCS, privateKeyCS)
                }

                else -> throw UnsupportedAlgorithmException()
            }
        }
    }
}

/** The VerificationKey class is the public half of a signing keypair. */
@Serializable
class VerificationKey private constructor(val key: CryptoString) : Verifier {

    private var publicHash: Hash? = null

    /**
     * Verifies the given signature for the data passed and returns true if it verifies and false if
     * it does not.
     *
     * @exception AlgorithmMismatchException Returned if the signature algorithm does not match that
     * of the SigningPair object.
     */
    override fun verify(data: ByteArray, signature: CryptoString): Boolean {
        if (data.isEmpty()) throw EmptyDataException()
        if (signature.prefix != key.prefix) throw AlgorithmMismatchException()

        val rawKey = key.toRaw()
        val box = Signature(rawKey, null)

        return box.detached_verify(data, signature.toRaw())

    }

    /** Interface override inherited from [PublicKey] */
    override fun getPublicKey(): CryptoString {
        return key
    }

    /**
     * Required function for the Verifier interface which returns a hash of the verification key.
     *
     * @exception UnsupportedAlgorithmException Returned if the library does not support the
     * algorithm specified by the keys
     */
    override fun getPublicHash(algorithm: CryptoType): Hash {
        if (publicHash == null || publicHash!!.prefix != algorithm.toString())
            publicHash = hash(key.toByteArray(), algorithm)
        return publicHash!!
    }

    override fun toString(): String {
        return key.toString()
    }

    companion object {

        /**
         * Creates a VerificationKey from the specified [CryptoString].
         *
         * @exception UnsupportedAlgorithmException Returned if the library does not support the
         * algorithm specified by the key
         */
        fun from(key: CryptoString): VerificationKey {

            if (!isSupportedSigning(key.prefix))
                throw UnsupportedAlgorithmException()

            return VerificationKey(key)
        }

        /**
         * Creates a Verification from a string containing data in [CryptoString] format.
         *
         * @exception BadValueException Returned if the key contains invalid data
         * @exception UnsupportedAlgorithmException Returned if the library does not support the
         * algorithm specified by the key string
         */
        fun fromString(s: String): VerificationKey {

            val cs = CryptoString.fromString(s)
                ?: throw BadValueException("bad public key")
            return from(cs)
        }
    }
}

/** Converts a CryptoString into a [VerificationKey] object */
fun CryptoString.toVerificationKey(): VerificationKey {
    return VerificationKey.from(this)
}

/**
 * Converts a KeyPair into a SigningPair or returns null if invalid
 */
fun KeyPair.toSigningPair(): SigningPair? {
    return this as? SigningPair
}
