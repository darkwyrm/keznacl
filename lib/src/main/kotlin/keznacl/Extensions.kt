package keznacl

/**
 * Executes the code block if the Boolean value is true
 */
inline fun Boolean.onTrue(block: () -> Unit) {
    if (this)
        block()
}

/**
 * Executes the code block if the Boolean value is true
 */
inline fun Boolean.onFalse(block: () -> Unit) {
    if (!this)
        block()
}

/**
 * Executes the code block if the string is empty or null
 */
inline fun CharSequence?.onNullOrEmpty(block: () -> Unit) {
    if (this.isNullOrEmpty())
        block()
}
