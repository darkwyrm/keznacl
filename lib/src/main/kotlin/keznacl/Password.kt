package keznacl

/**
 * An enum class to quickly figure out parameters for a hash function. Basic, despite the usage by
 * GenZ, is good, basic protection, and the other levels are for situations which require more.
 */
enum class HashStrength {
    Basic,
    Extra,
    Secret,
    Extreme,
}

/**
 * Data class used for passing around information about a password without including the hashed
 * password itself.
 */
open class PasswordInfo(
    open var algorithm: String = "",
    open var salt: String = "",
    open var parameters: String = ""
) {
    override fun toString(): String {
        return "\$$algorithm\$$parameters\$$salt"
    }

    fun copy(): PasswordInfo {
        return PasswordInfo(algorithm, salt, parameters)
    }
}

/** Parent class for all password hashing algorithms. */
abstract class Password {

    protected open var info = PasswordInfo()
    protected open var hashValue = ""

    /** Returns the hash of the supplied password */
    open fun getHash(): String {
        return hashValue
    }

    override fun toString(): String {
        return hashValue
    }

    /** Returns the name of the password algorithm used */
    open fun getAlgorithm(): String {
        return info.algorithm
    }

    /** Internal method for child classes to set the password algorithm information */
    protected open fun setAlgorithm(value: String) {
        info.algorithm = value
    }

    /** Returns a string containing the password algorithm's parameters and their values */
    open fun getParameters(): String {
        return info.parameters
    }

    /**
     * Sets the parameter string for the password hash
     *
     * @exception BadValueException Returned if given an invalid parameter string
     */
    open fun setParameters(value: String) {
        info.parameters = value
    }

    /** Returns the Base64-encoded salt used */
    open fun getSalt(): String {
        return info.salt
    }

    /**
     * Sets the Base64-encoded string which is used for the password hash's salt. This call isn't
     * generally used except in very specific circumstances.
     */
    open fun setSalt(value: String) {
        info.salt = value
    }

    /** Returns a copy of the current Password object */
    abstract fun copy(): Password

    /**
     * Sets the Password value from an existing password hash.
     *
     * @exception BadValueException Returned if invalid data is passed
     */
    abstract fun setFromHash(hashStr: String)

    /**
     * Sets parameters for the algorithm based on the strength requested. The specific parameter
     * values assigned to a strength level is specific to the algorithm.
     */
    abstract fun setStrength(strength: HashStrength): Password

    /** Creates a new hash given a cleartext password. */
    abstract fun updateHash(pw: String): String

    /** Checks the cleartext password against the object's hash and returns true if they match */
    abstract fun verify(pw: String): Boolean
}

/**
 * Returns the password hashing algorithms supported by the library. Currently the supported
 * algorithms are Argon2id, Argon2d, Argon2i, and Sha3-256.
 */
fun getSupportedPasswordAlgorithms(): List<String> {
    return listOf("ARGON2D", "ARGON2I", "ARGON2ID", "SHA3-256")
}

/**
 * Returns the recommended password hashing algorithm supported by the library. If you don't
 * know what to choose for your implementation, this will provide a good default which balances
 * speed and security.
 */
fun getPreferredPasswordAlgorithm(): String {
    return "ARGON2ID"
}

/**
 * Returns an instance of a password hashing class given the name of the algorithm. Returns null if
 * there is no hashing class by the name given.
 */
fun getHasherForAlgorithm(algorithm: String): Password? {
    return when (algorithm) {
        "ARGON2D" -> Argon2dPassword()
        "ARGON2I" -> Argon2iPassword()
        "ARGON2ID" -> Argon2idPassword()
        "SHA3-256" -> SHAPassword()
        else -> null
    }
}

/**
 * Validates the data in the PasswordInfo instance passed to it. This call ensures that (a) the
 * algorithm used is supported by the library and (b) all required fields for hashing are present
 * and valid.
 */
fun validatePasswordInfo(pi: PasswordInfo) {

    val upperAlgo = pi.algorithm.uppercase()
    if (!getSupportedPasswordAlgorithms().contains(upperAlgo))
        throw UnsupportedAlgorithmException()

    when (upperAlgo) {
        "ARGON2D", "ARGON2I", "ARGON2ID" -> Argon2Password.validateInfo(pi)
        "SHA3-256" -> SHAPassword.validateInfo(pi)
        else -> throw UnsupportedAlgorithmException("${pi.algorithm} not supported")
    }
}

/**
 * Validates the passed password info and returns the Password subclass, enabling the caller to
 * hash a password using the process specified in the PasswordInfo instance.
 */
fun passhasherForInfo(pi: PasswordInfo): Password {
    validatePasswordInfo(pi)

    return when (pi.algorithm.uppercase()) {
        "ARGON2D" -> {
            Argon2dPassword().let {
                it.setFromInfo(pi)
                it
            }
        }

        "ARGON2I" -> {
            Argon2iPassword().let {
                it.setFromInfo(pi)
                it
            }
        }

        "ARGON2ID" -> {
            Argon2idPassword().let {
                it.setFromInfo(pi)
                it
            }
        }

        "SHA3-256" -> {
            SHAPassword().let {
                it.setFromInfo(pi)
                it
            }
        }

        else -> {
            throw ProgramException(
                "Unreachable code in passhasherForInfo reached. " +
                        "Algorithm: ${pi.algorithm.uppercase()}"
            )
        }
    }
}

/**
 * Given a $-delimited hash string, attempt to return a Password subclass which handles the
 * specified algorithm. The returned object is initialized with the hash and is ready for use.
 *
 * @exception BadValueException Returned if given a bad hash string
 * @exception UnsupportedAlgorithmException Returned if the algorithm used by the hash is not
 * supported
 */
fun getHasherForHash(s: String): Password {
    val parts = s.trim().split('$').filter { it.isNotEmpty() }
    if (parts.size < 2) throw BadValueException("Invalid hash")
    val algo = parts[0].uppercase()
    val out = getHasherForAlgorithm(algo)
        ?: throw UnsupportedAlgorithmException("Algorithm $algo not supported")
    out.setFromHash(s)
    return out
}
