package keznacl

import com.iwebpp.crypto.TweetNaclFast
import kotlinx.serialization.Serializable

/** Returns true if the string represents a supported asymmetric encryption algorithm */
fun isSupportedAsymmetric(s: String): Boolean {
    return s.uppercase() == "CURVE25519"
}

/**
 * Returns the asymmetric encryption algorithms supported by the library. Currently the only
 * supported algorithm is CURVE25519, which is included in drafts for FIPS 186-5. Other algorithms
 * may be added at a future time.
 */
fun getSupportedAsymmetricAlgorithms(): List<CryptoType> {
    return listOf(CryptoType.CURVE25519)
}

/**
 * Returns the recommended asymmetric encryption algorithm supported by the library. If you don't
 * know what to choose for your implementation, this will provide a good default which balances
 * speed and security.
 */
fun getPreferredAsymmetricAlgorithm(): CryptoType {
    return CryptoType.CURVE25519
}

/**
 * The EncryptionPair class represents a pair of asymmetric encryption keys. It is designed to make
 * managing keys, encrypting/decrypting data, and debugging easier. Usage is pretty simple:
 * getPublicKey(), getPrivateKey(), decrypt(), and encrypt(). Instantiation can be done with from(),
 * fromStrings(), or generate().
 */
@Serializable
class EncryptionPair private constructor(
    val pubKey: CryptoString,
    val privKey: CryptoString
) : Encryptor, Decryptor, KeyPair {

    override fun getPublicKey(): CryptoString {
        return pubKey
    }

    override fun getPrivateKey(): CryptoString {
        return privKey
    }

    override fun getPublicHash(algorithm: CryptoType): Hash {
        return pubKey.hash(algorithm)
    }

    override fun getPrivateHash(algorithm: CryptoType): Hash {
        return privKey.hash(algorithm)
    }

    /**
     * Encrypts the passed data into a [CryptoString].
     *
     * @exception IllegalArgumentException Returned if there was a decoding error
     */
    override fun encrypt(data: ByteArray): CryptoString {
        val rawKey = pubKey.toRaw()
        val box = SealedBox()
        val result = box.cryptoBoxSeal(data, rawKey)

        return CryptoString.fromBytes(CryptoType.CURVE25519, result)!!
    }

    /**
     * Decrypts the data passed into a [ByteArray].
     *
     * @exception IllegalArgumentException Returned if there was a decoding error
     */
    override fun decrypt(encData: CryptoString): ByteArray {
        val ciphertext = encData.toRaw()
        val box = SealedBox()
        return box.cryptoBoxSealOpen(
            ciphertext,
            pubKey.toRaw(),
            privKey.toRaw()
        )
    }

    override fun toString(): String {
        return "$pubKey,$privKey"
    }

    companion object {

        /**
         * Creates an EncryptionPair from the specified CryptoString objects.
         *
         * @exception KeyErrorException Returned if given keys using different algorithms
         * @exception UnsupportedAlgorithmException Returned if the library does not support the
         * algorithm specified by the keys
         */
        fun from(pubKeyCS: CryptoString, privKeyCS: CryptoString): EncryptionPair {

            if (pubKeyCS.prefix != privKeyCS.prefix)
                throw KeyErrorException()
            if (!isSupportedAsymmetric(pubKeyCS.prefix))
                throw UnsupportedAlgorithmException()

            return EncryptionPair(pubKeyCS, privKeyCS)
        }

        /**
         * Creates an EncryptionPair from two strings containing data in [CryptoString] format.
         *
         * @exception BadValueException Returned if either key contains invalid data
         * @exception KeyErrorException Returned if given keys using different algorithms
         * @exception UnsupportedAlgorithmException Returned if the library does not support the
         * algorithm specified by the keys
         */
        fun fromStrings(pubKeyStr: String, privKeyStr: String): EncryptionPair {
            val pubKey = CryptoString.fromString(pubKeyStr)
                ?: throw BadValueException("bad public key")
            val privKey = CryptoString.fromString(privKeyStr)
                ?: throw BadValueException("bad private key")
            return from(pubKey, privKey)
        }

        /**
         * Generates a new asymmetric encryption keypair using the specified algorithm.
         *
         * @exception KeyErrorException Returned if there was a problem generating the keypair
         * @exception UnsupportedAlgorithmException Returned if the library does not support the
         * algorithm specified
         */
        fun generate(algorithm: CryptoType = getPreferredAsymmetricAlgorithm()): EncryptionPair {

            when (algorithm) {
                CryptoType.CURVE25519 -> {
                    val keyPair = TweetNaclFast.Box.keyPair()
                    val pubKey = CryptoString.fromBytes(CryptoType.CURVE25519, keyPair.publicKey)
                        ?: throw KeyErrorException()

                    val privKey = CryptoString.fromBytes(CryptoType.CURVE25519, keyPair.secretKey)
                        ?: throw KeyErrorException()

                    return from(pubKey, privKey)
                }

                else -> throw UnsupportedAlgorithmException()
            }
        }

    }
}

/**
 * The EncryptionKey class represents just an encryption key without its corresponding
 * decryption key.
 */
class EncryptionKey private constructor(val key: CryptoString) : Encryptor {

    private var publicHash: Hash? = null

    /**
     * Encrypts the passed data and returns the encrypted data encoded as a [CryptoString].
     *
     * @exception IllegalArgumentException Returned if there was a decoding error
     */
    override fun encrypt(data: ByteArray): CryptoString {
        val rawKey = key.toRaw()
        val encrypted = SealedBox().cryptoBoxSeal(data, rawKey)


        return CryptoString.fromBytes(CryptoType.CURVE25519, encrypted)!!
    }

    /** Interface override inherited from [PublicKey] */
    override fun getPublicKey(): CryptoString {
        return key
    }

    /** Interface override inherited from [PublicKey] */
    override fun getPublicHash(algorithm: CryptoType): Hash {
        if (publicHash == null || publicHash!!.prefix != algorithm.toString())
            publicHash = hash(key.toByteArray(), algorithm)
        return publicHash!!
    }

    override fun toString(): String {
        return key.toString()
    }

    companion object {

        /**
         * Creates an EncryptionKey from the specified [CryptoString].
         *
         * @exception UnsupportedAlgorithmException Returned if the library does not support the
         * algorithm specified by the key
         */
        fun from(pubKey: CryptoString): EncryptionKey {

            if (!isSupportedAsymmetric(pubKey.prefix))
                throw UnsupportedAlgorithmException()

            return EncryptionKey(pubKey)
        }

        /**
         * Creates an EncryptionPair from two strings containing data in [CryptoString] format.
         *
         * @exception BadValueException Returned if either key contains invalid data
         * @exception UnsupportedAlgorithmException Returned if the library does not support the
         * algorithm specified by the key string
         */
        fun fromString(s: String): EncryptionKey {

            val pubKey = CryptoString.fromString(s)
                ?: throw BadValueException("bad public key")

            return from(pubKey)
        }
    }
}

/**
 * Converts a CryptoString into an [EncryptionKey] object
 *
 * @exception UnsupportedAlgorithmException Returned if the library does not support the
 * algorithm specified by the key
 */
fun CryptoString.toEncryptionKey(): EncryptionKey {
    return EncryptionKey.from(this)
}

/**
 * Converts a KeyPair into an EncryptionPair or returns null if invalid
 */
fun KeyPair.toEncryptionPair(): EncryptionPair? {
    return this as? EncryptionPair
}
