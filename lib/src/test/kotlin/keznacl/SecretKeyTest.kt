package keznacl

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class SecretKeyTest {

    @Test
    fun testSymmetricSupport() {
        assert(isSupportedAlgorithm("XSALSA20"))
        val symSupport = getSupportedSymmetricAlgorithms()
        assertEquals(1, symSupport.size)
        assertEquals(CryptoType.XSALSA20, symSupport[0])

        assertEquals(CryptoType.XSALSA20, getPreferredSymmetricAlgorithm())
    }

    @Test
    fun testSecretKey() {
        val testdata = "This is some test data"
        val key =
            SecretKey.fromString("XSALSA20:Z%_Is*V6uc!_+QIG5F`UJ*cLYoO`=58RCuAk-`Bq")
        assertEquals(CryptoType.XSALSA20, key.getType())

        val encdata = key.encrypt(testdata.toByteArray())
        key.decrypt(encdata)

        assertEquals(key.key.value, "XSALSA20:Z%_Is*V6uc!_+QIG5F`UJ*cLYoO`=58RCuAk-`Bq")

        val key2 = SecretKey.generate(CryptoType.XSALSA20)
        val encdata2 = key2.encrypt(testdata.toByteArray())
        key2.decrypt(encdata2)

        // Lint removal / smoke test
        CryptoString.fromString("XSALSA20:Z%_Is*V6uc!_+QIG5F`UJ*cLYoO`=58RCuAk-`Bq")!!
            .toSecretKey()
    }

}