package keznacl

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class EncryptionTest {

    @Test
    fun algorithmSupportTest() {

        assert(isSupportedAlgorithm("CURVE25519"))
        val enc = getSupportedAsymmetricAlgorithms()
        assertEquals(1, enc.size)
        assertEquals(CryptoType.CURVE25519, enc[0])

        assertEquals(CryptoType.CURVE25519, getPreferredAsymmetricAlgorithm())
    }

    @Test
    fun pairEncryptTest() {
        val keypair = EncryptionPair.fromStrings(
            "CURVE25519:(B2XX5|<+lOSR>_0mQ=KX4o<aOvXe6M`Z5ldINd`",
            "CURVE25519:(Rj5)mmd1|YqlLCUP0vE;YZ#o;tJxtlAIzmPD7b&"
        )
        assertEquals(CryptoType.CURVE25519, keypair.getType())

        assertEquals(keypair.pubKey.value, "CURVE25519:(B2XX5|<+lOSR>_0mQ=KX4o<aOvXe6M`Z5ldINd`")
        assertEquals(
            keypair.privKey.value,
            "CURVE25519:(Rj5)mmd1|YqlLCUP0vE;YZ#o;tJxtlAIzmPD7b&"
        )

        val testdata = "This is some encryption test data"
        val encdata = keypair.encrypt(testdata.toByteArray())
        keypair.decrypt(encdata)

        val keypair2 = EncryptionPair.generate(CryptoType.CURVE25519)
        val encdata2 = keypair2.encrypt(testdata.toByteArray())
        keypair2.decrypt(encdata2)

        // Smoke test / lint removal
        keypair.getPublicHash()
        CryptoString.fromString("CURVE25519:(B2XX5|<+lOSR>_0mQ=KX4o<aOvXe6M`Z5ldINd`")!!
            .toEncryptionKey()

        EncryptionPair.from(
            CryptoString.fromString("CURVE25519:(B2XX5|<+lOSR>_0mQ=KX4o<aOvXe6M`Z5ldINd`")!!,
            CryptoString.fromString("CURVE25519:(Rj5)mmd1|YqlLCUP0vE;YZ#o;tJxtlAIzmPD7b&")!!
        )
            .toEncryptionPair()!! // just for removing a warning ;)
    }

    @Test
    fun wrapTest() {
        val keypair = EncryptionPair.fromStrings(
            "CURVE25519:(B2XX5|<+lOSR>_0mQ=KX4o<aOvXe6M`Z5ldINd`",
            "CURVE25519:(Rj5)mmd1|YqlLCUP0vE;YZ#o;tJxtlAIzmPD7b&"
        )
        val key =
            SecretKey.fromString("XSALSA20:Z%_Is*V6uc!_+QIG5F`UJ*cLYoO`=58RCuAk-`Bq")


        val wrapped = keypair.wrap(key)
        keypair.unwrap(wrapped)
    }

    @Test
    fun keyEncryptTest() {
        val keypair = EncryptionPair.fromStrings(
            "CURVE25519:(B2XX5|<+lOSR>_0mQ=KX4o<aOvXe6M`Z5ldINd`",
            "CURVE25519:(Rj5)mmd1|YqlLCUP0vE;YZ#o;tJxtlAIzmPD7b&"
        )

        // Because fromString() calls from() internally, we use this less-efficient construction
        // to ensure both functions are tested
        val key = EncryptionKey.fromString(keypair.pubKey.toString())
        assertEquals(CryptoType.CURVE25519, key.getType())
        assertEquals(key.key.value, "CURVE25519:(B2XX5|<+lOSR>_0mQ=KX4o<aOvXe6M`Z5ldINd`")

        val testdata = "This is some encryption test data"
        val encdata = key.encrypt(testdata.toByteArray())
        keypair.decrypt(encdata)
    }
}